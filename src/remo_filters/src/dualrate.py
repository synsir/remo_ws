from basefilter import BaseFilter
from math import pow


class Filter(BaseFilter):
    def __init__(self):
        BaseFilter.__init__(self, "Dualrate")

    def _impl_function(self, channel, mod):
        if channel > 0.0:
            return channel * mod[0]
        else:
            return channel * mod[1]

    def _impl_getDefaultCfg(self, channel):
        return [80, 80]

    def _impl_afterConfigLoad(self):
        # after load, the config contains percentage scaling values as integer (+-0..130)
        # modify it to match the real scale of float -1.0 .. 1.0
        # to convert the output of this function to the final pwm signal, also multiply by 384.615
        # this roughly gives 500 usecond in either direction at full dualrate of 130
        self._mods = [((float(x) / 100.0 * 384.615), (float(y) / 100.0 * 384.615))
                      for x, y in self._mods]

    def _impl_getCfgDescription(self):
        return "Array of 2 values between about -130 and 130, or more, there is no limit. negative values invert the servo direction"
