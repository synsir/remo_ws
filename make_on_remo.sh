#!/bin/bash

echo "Rsyncing workspace to remote device..."
echo ""
echo ""
echo ""
#exclude is a list, add comma seperated strings
rsync -avh --delete --exclude={'.git'} src/ remo:~/remo_ws/src/
echo ""
echo ""
echo ""
echo "Rebuilding the catkin workspace..."
echo ""
echo ""
echo ""
ssh remo "cd ~/remo_ws && source ./devel/setup.zsh && catkin_make"