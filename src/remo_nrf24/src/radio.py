from RF24 import *

device = None

class Radio:
  def __init__(self, this_address):
    radio = RF24(25, 0)
    radio.begin()
    radio.setAddressWidth(4)
    radio.enableDynamicPayloads()
    radio.enableAckPayload()
    radio.setRetries(3, 10)
    radio.setChannel(78)
    radio.openReadingPipe(1, this_address)
    radio.printDetails()
    self._radio = radio
    self._counter_recv_total = 0
    self._counter_send_total = 0
    self._counter_send_fails = 0
  
  def set_destination(self, address):
    self._radio.openWritingPipe(address)

  def listen(self):
    self._radio.startListening()

  def stop_listem(self):
    self._radio.stopListening()

  def write(self, data):
    self._counter_send_total += 1
    res = self._radio.write(data)
    if res == False:
      self._radio.flush_tx()
      self._counter_send_fails += 1
    return res

  def write_mic(self, data):
    # calculate MIC
    return self.write(data)