import os, re

def main():
    msgs = getFilesString('msg', r'\w+\.msg\b')
    srvs = getFilesString('srv', r'\w+\.srv\b')
    with open('CMakeLists_template.txt', 'r') as f:
        content = f.read()
        content = content.format(**{'msg_files':msgs,'srv_files': srvs})
    with open('CMakeLists.txt', 'w') as fo:
        fo.write(content)

def getFilesString(dirname, regex):
    outstr = ""
    reg = re.compile(regex)
    for root, dirs, files in os.walk(dirname):
        for f in files:
            if re.match(reg, f):
                outstr = outstr + "\n  " + f
        break # no subdirectorys need to be scanned right
    return outstr

main()
