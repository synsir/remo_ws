from remo_launcher.msg import IntermediateChannels
import rospy
import json
from os import path, makedirs

class BaseFilter:
  def __init__(self, name):
    self._name = name
    self._count = IntermediateChannels.CHANNELS_COUNT
    self._mods = [2.0] * IntermediateChannels.CHANNELS_COUNT

  def apply(self, input_channels):
    output = []
    for chan, mod in zip(input_channels, self._mods):
      output.append(self._impl_function(chan, mod))
    return output

  def hasParent(self):
    """returns true if this node is completely dependant on the parent input

    Returns:
        bool: usualy true. only the generator node has n oparent as of now
    """
    return True

  def loadConfig(self, modelname):
    configfolder = path.expanduser("~/remo_configs/" + modelname)
    configfile = configfolder + "/filter.json"
    try:
      with open(configfile) as f:
        cfg = json.load(f)
    except:
      rospy.logwarn("Couldn't open filter config file, creating new one")
      cfg = {}
      if not path.exists(configfolder):
        makedirs(configfolder)
      with open(configfile, mode='w') as f:
        json.dump(cfg, f)
    try:
      if self._name not in cfg:
        rospy.logwarn("Filter not found in config, adding default values")
        cfg[self._name] = {}
        for idx in range(self._count): 
          cfg[self._name]["ch_" + str(idx)] = self._impl_getDefaultCfg(idx)
        cfg[self._name]["description"] = self._impl_getCfgDescription()
        with open(configfile, mode='w') as f:
          json.dump(cfg, f, sort_keys=True, indent=2)      
      for idx in range(self._count):
        key = "ch_" + str(idx)
        if key not in cfg[self._name]:
          rospy.logwarn("Filter for channel {} not found in config, adding default value".format(idx))
          cfg[self._name][key] = self._impl_getDefaultCfg(idx)
          with open(configfile, mode='w') as f:
            json.dump(cfg, f, sort_keys=True, indent=2)
        self._mods[idx] = cfg[self._name][key]
      self._impl_afterConfigLoad()
    except Exception as e:
      rospy.logerr("Couldn't complete loading filter config: " + str(e))

  def _impl_function(self, channel, mod):
    """implementation of the actual function of this node

    Args:
        channel (float): the input value of this channel
        mod (any): the config value stored for the particular channel

    Returns:
        float: the result of the channel
    """
    return channel * mod

  def _impl_getDefaultCfg(self, channel_idx):
    """should return a sensible value for the given channel as a default to initialize the config

    Args:
        channel_idx (int): the index of the channel to provide a default for

    Returns:
        any: json serializable structure, int, list, dict, anything ok
    """
    return 2.0

  def _impl_afterConfigLoad(self):
    """ this function is called after a config load or update and should initialize anything neccessairy
    to use the new values stored as it's config.
    """
    pass

  def _impl_getCfgDescription(self):
    """should return a string that gets passed to the config as a description of the possible values
    """
    return "Basefilter description: Descendant class did not implement this yet."
  
