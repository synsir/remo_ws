from basefilter import BaseFilter
from sensor_msgs.msg import Joy
from remo_launcher.msg import IntermediateChannels
from math import pow
import rospy


class Filter(BaseFilter):
  def __init__(self):
    BaseFilter.__init__(self, "Joyinput")
    self._outpub = None

  def hasParent(self):
    return False

  def onJoyMsg(self, msg):
    outmsg = IntermediateChannels()
    for i in range(outmsg.CHANNELS_COUNT):
      outmsg.channels[i] = msg.axes[self._mods[i]]
    outmsg.header.stamp = rospy.Time.now()
    msg.header.frame_id = self._name
    self._outpub.publish(outmsg)

  def doAction(self, outpub):
    self._outpub = outpub
    rospy.Subscriber("/joy", Joy, self.onJoyMsg, queue_size=1)
    # since we actually have a parent, don't return
    rospy.spin()

  def _impl_getDefaultCfg(self, channel):
    # by default, return the setting I like, similar to RC remotes
    mapping = [
        1,
        0,
        4,
        3,
        2,
        5
    ]
    return mapping[channel]

  def _impl_getCfgDescription(self):
    return "Convert sensor_msgs/Joy to IntermediateChannel message. Listens to /joy topic"
