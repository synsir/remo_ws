#pragma once

#include <sensor_msgs/Joy.h>
#include <remo_launcher/IntermediateChannels.h>
#include <ros/ros.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <mutex>
#include <cassert>
#include <fstream>
#include "json.hpp"
#include <sstream>
#include <cmath>

class Filter
{
public:
    Filter(const std::string &name, ros::Publisher &pub)
        : _outpub(pub)
    {
        _name = name;
        _mods.resize(remo_launcher::IntermediateChannels::CHANNELS_COUNT);
        for (auto &ch : _mods)
        {
            ch.push_back(1.337);
        }
        _outmsg.header.frame_id = name;
    }

    virtual ~Filter()
    {
        ROS_INFO("Filter destroyed: %s", _name.c_str());
    }

    static const char *name()
    {
        return "base_filter";
    }

    void load_config(const std::string &modelname)
    {
        std::string cfgfolder = getenv("HOME");
        cfgfolder += "/remo_configs/" + modelname + "/";
        _configfile = cfgfolder + _name + ".json";
        std::ifstream infile(_configfile);
        bool config_changed = false;
        if (infile.is_open())
        {
            infile >> _config;
            infile.close();
        }
        else
        {
            ROS_INFO("Could not find filter config: %s. Creating new one", _configfile.c_str());
        }
        // check all values present in config
        if (!_config.contains("description"))
        {
            _config["description"] = _impl_get_config_description();
            config_changed = true;
        }
        for (int i = 0; i < remo_launcher::IntermediateChannels::CHANNELS_COUNT; ++i)
        {
            std::stringstream ss;
            ss << "ch_" << i + 1;
            if (!_config.contains(ss.str()))
            {
                _config[ss.str()] = _impl_get_default_config(i);
                config_changed = true;
            }
        }
        if (config_changed)
        {
            std::ofstream outfile(_configfile);
            if (outfile.is_open())
            {
                outfile << _config.dump(4);
                outfile.close();
                ROS_INFO("Written new config filter file: %s", _configfile.c_str());
            }
            else
            {
                ROS_ERROR("Couldn't open filter config file for writing: %s", _configfile.c_str());
            }
        }
        // run any neccessairy modifications to the config
        for (uint32_t i = 0; i < remo_launcher::IntermediateChannels::CHANNELS_COUNT; ++i)
        {
            std::stringstream ss;
            ss << "ch_" << i + 1;
            _impl_after_config_load(_mods[i], _config[ss.str()]);
        }
    }

    void apply(const remo_launcher::IntermediateChannelsConstPtr &pmsg)
    {
        for (uint32_t i = 0; i < remo_launcher::IntermediateChannels::CHANNELS_COUNT; ++i)
        {
            _outmsg.channels[i] = _impl_apply(pmsg->channels[i], _mods[i]);
        }
        _outmsg.header.stamp = ros::Time::now();
        _outpub.publish(_outmsg);
    }

    void apply_no_pub(remo_launcher::IntermediateChannels& msg)
    {
        for (uint32_t i = 0; i < remo_launcher::IntermediateChannels::CHANNELS_COUNT; ++i)
        {
            msg.channels[i] = _impl_apply(msg.channels[i], _mods[i]);
        }
    }

    virtual bool hasParent()
    {
        return true;
    }

    virtual void generator_entry(ros::NodeHandle& n, std::vector<std::unique_ptr<Filter>>& filterlist)
    {
    }

    virtual float _impl_apply(float chan, const std::vector<float> &mods)
    {
        return chan * mods[0];
    }

    virtual std::vector<float> _impl_get_default_config(uint32_t channel_index)
    {
        return {1.337};
    }

    virtual void _impl_after_config_load(std::vector<float>& mod, nlohmann::json& cfg)
    {
        ROS_WARN("_impl_after_config_load not realized! this is could be a problem for node: %s", _name.c_str());
        // after loading, the _conifg is populated, it's the implementations job to untangle the values into _mods
        mod[0] = cfg[0];
    }

    virtual const char *_impl_get_config_description()
    {
        return "Basefilter description: Descendant class did not implement this yet.";
    }

protected:
    typedef std::vector<std::vector<float>> modlist_t;
    remo_launcher::IntermediateChannels _outmsg;
    modlist_t _mods;
    ros::Publisher _outpub;
    std::string _name;
    std::string _configfile;
    nlohmann::json _config;
};

class JoyInput : public Filter
{
public:
    JoyInput(const std::string &name, ros::Publisher &pub)
        : Filter(name, pub)
    {
        _pfilterlist = nullptr;
    }

    static const char *name()
    {
        return "joyinput";
    }

    virtual bool hasParent()
    {
        return false;
    }

    void on_joy_msg(const sensor_msgs::JoyConstPtr &pmsg)
    {
        int i = 0;
        for(auto &mod : _mods)
        {
            int chan(mod[0]);
            _outmsg.channels[i++] = pmsg->axes[chan];
        }
        _outmsg.header.stamp = ros::Time::now();
        _outpub.publish(_outmsg);
    }

    void on_joy_msg_ndlt(const sensor_msgs::JoyConstPtr &pmsg)
    {
        int i = 0;
        for(auto &mod : _mods)
        {
            int chan(mod[0]);
            _outmsg.channels[i++] = pmsg->axes[chan];
        }
        for(auto &pf : *_pfilterlist)
        {
            pf->apply_no_pub(_outmsg);
        }
        _outmsg.header.stamp = ros::Time::now();
        _outpub.publish(_outmsg);
    }

    virtual void generator_entry(ros::NodeHandle& n, std::vector<std::unique_ptr<Filter>>& filterlist)
    {
        if(filterlist.size() > 0)
        {
            // nodelet mode, link alternative on_msg
            _pfilterlist = &filterlist;
            _insub = n.subscribe<sensor_msgs::Joy>("/joy", 1,
                                               boost::bind(&JoyInput::on_joy_msg_ndlt, boost::ref(*this), _1));
        }
        else
        {
            _insub = n.subscribe<sensor_msgs::Joy>("/joy", 1,
                                               boost::bind(&JoyInput::on_joy_msg, boost::ref(*this), _1));
        }
    }

    virtual float _impl_apply(float chan, const std::vector<float> &mods)
    {
        return chan;
    }

    virtual std::vector<float> _impl_get_default_config(uint32_t channel_index)
    {
        float mapping[] = {1, 0, 4, 3, 2, 5};
        return {mapping[channel_index]};
    }

    virtual void _impl_after_config_load(std::vector<float>& mod, nlohmann::json& cfg)
    {
        // after loading, the _conifg is populated, it's the implementations job to untangle the values into _mods
        mod[0] = cfg[0];
    }

    virtual const char *_impl_get_config_description()
    {
        return "Convert sensor_msgs/Joy to IntermediateChannel message. Listens to /joy topic";
    }

private:
    ros::Subscriber _insub;
    std::vector<std::unique_ptr<Filter>> *_pfilterlist;
};

class Expo : public Filter
{
public:
    Expo(const std::string &name, ros::Publisher &pub)
        : Filter(name, pub)
    {
    }

    static const char *name()
    {
        return "expo";
    }

    virtual float _impl_apply(float chan, const std::vector<float> &mods)
    {
        // K: 1, 1.5, 2, 3, 5...
        // X: -1 .. 1
        // y = (x*x*x * (k-1) + x ) / k
        float mod = mods[0];
        return (pow(chan, 3) * (mod - 1.0) + chan) / mod;
    }

    virtual std::vector<float> _impl_get_default_config(uint32_t channel_index)
    {
        return {20};
    }

    virtual void _impl_after_config_load(std::vector<float>& mod, nlohmann::json& cfg)
    {
        float val = cfg[0].get<float>() / 100.0;
        val = 4*val*val + 1.0;
        mod[0] = val;
    }

    virtual const char *_impl_get_config_description()
    {
        return "Value between 0 and 100";
    }
};

class Trim : public Filter
{
public:
    Trim(const std::string &name, ros::Publisher &pub)
        : Filter(name, pub)
    {
    }

    static const char *name()
    {
        return "trim";
    }

    virtual float _impl_apply(float chan, const std::vector<float> &mods)
    {
        return chan + mods[0];
    }

    virtual std::vector<float> _impl_get_default_config(uint32_t channel_index)
    {
        return {0};
    }

    virtual void _impl_after_config_load(std::vector<float>& mod, nlohmann::json& cfg)
    {
        // after load, the config contains percentage offset values as integer (+-0..1000)
        // modify it to match the real scale of float -1.0 .. 1.0
        //self._mods = [ float(x) / 1000.0 for x in self._mods ]
        float val = cfg[0];
        val = val / 1000.0;
        mod[0] = val;
    }

    virtual const char *_impl_get_config_description()
    {
        return "Value between -1000 and 1000";
    }
};

class DualRate : public Filter
{
public:
    DualRate(const std::string &name, ros::Publisher &pub)
        : Filter(name, pub)
    {
    }

    static const char *name()
    {
        return "dualrate";
    }

    virtual float _impl_apply(float chan, const std::vector<float> &mods)
    {
        if(chan > 0.0)
            return chan * mods[0] + 1500.0;
        return chan * mods[1] + 1500.0;
    }

    virtual std::vector<float> _impl_get_default_config(uint32_t channel_index)
    {
        return {80.0, 80.0};
    }

    virtual void _impl_after_config_load(std::vector<float>& mod, nlohmann::json& cfg)
    {
        // after load, the config contains percentage scaling values as integer (+-0..130)
        // modify it to match the real scale of float -1.0 .. 1.0
        // to convert the output of this function to the final pwm signal, also multiply by 384.615
        // this roughly gives 500 usecond in either direction at full dualrate of 130
        //self._mods = [((float(x) / 100.0 * 384.615), (float(y) / 100.0 * 384.615))
        //              for x, y in self._mods]
        mod[0] = _convert_value(cfg[0]);
        mod[1] = _convert_value(cfg[1]);
    }

    static float _convert_value(float val)
    {
        return val / 100.0 * 384.615;
    }

    virtual const char *_impl_get_config_description()
    {
        return "Array of 2 values (first is pos, second is neg range) between about -130 and 130, or more, there is no limit. negative values invert the servo direction.";
    }
};