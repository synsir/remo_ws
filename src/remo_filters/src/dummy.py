from basefilter import BaseFilter
from remo_launcher.msg import IntermediateChannels
import rospy

class Generator(BaseFilter):
  def __init__(self):
    BaseFilter.__init__(self, "Generator")
    self._mods = [0.05] * self._count
    self._values = [0.0] * self._count
    self._index = 0

  def hasParent(self):
    return False

  def doAction(self, outpub):
    msg = IntermediateChannels()
    msg.channels = self.apply(range(self._count))
    msg.header.frame_id = self._name
    msg.header.stamp = rospy.Time.now()
    outpub.publish(msg)

  def _impl_function(self, channel, mod):
    if self._index < self._count:
      # index == 0 .. count-1
      self._values[self._index] += mod
      retval = self._values[self._index]
      self._index += 1
      return retval
    else:
      # index == count
      self._index = 0
      if self._mods[0] > 0.0 and self._values[0] >= 1.0:
        # currently counting up and over limit
        self._mods = [-0.05] * self._count
      elif self._mods[0] < 0.0 and self._values[0] <= -1.0:
        # currently counting down and under limit
        self._mods = [0.05] * self._count
      return self._impl_function(channel, mod)

  def _impl_getDefaultCfg(self, channel_idx):
    return 0.05
