#!/bin/bash

WORKSPACE=$1

LOGFILE="$HOME/remo_boot_log.txt"

source /opt/ros/melodic/setup.bash &> $LOGFILE
source "$WORKSPACE/devel/setup.bash" &>> $LOGFILE


MACHINE_IP=(`hostname -I`) &>> $LOGFILE
MACHINE_IP=${MACHINE_IP[0]}  &>> $LOGFILE
if [ "$#" == "1" ]; then
	MASTER_IP=$MACHINE_IP
	roscore & &>> $LOGFILE
else
	MASTER_IP=$2
fi


export ROS_IP=$MACHINE_IP &>> $LOGFILE
export ROS_MASTER_URI="http://$MASTER_IP:11311" &>> $LOGFILE


echo "ROS IP: $ROS_IP" &>> $LOGFILE
echo "ROS Master URI: $ROS_MASTER_URI" &>> $LOGFILE

roslaunch --wait remo_launcher remo.launch &>> $LOGFILE

