import subprocess


def shutdown():
    cmd = "sudo shutdown now && pkill roslaunch"
    subprocess.check_output(cmd, shell=True)
