import subprocess

# Shell scripts for system monitoring from here : https://unix.stackexchange.com/questions/119126/command-to-display-memory-usage-disk-usage-and-cpu-load
# cmd = "hostname -I | cut -d\' \' -f1"
# IP = subprocess.check_output(cmd, shell=True)
# cmd = "top -bn1 | grep load | awk '{printf \"CPU Load: %.2f\", $(NF-2)}'"
# CPU = subprocess.check_output(cmd, shell=True)
# cmd = "free -m | awk 'NR==2{printf \"Mem: %s/%sMB %.2f%%\", $3,$2,$3*100/$2 }'"
# MemUsage = subprocess.check_output(cmd, shell=True)
# cmd = "df -h | awk '$NF==\"/\"{printf \"Disk: %d/%dGB %s\", $3,$2,$5}'"
# Disk = subprocess.check_output(cmd, shell=True)

def get_ip():
  cmd = "hostname -I | cut -d\' \' -f1"
  return str(subprocess.check_output(cmd, shell=True))

def get_cpu():
  cmd = "top -bn1 | grep load | awk '{printf \"%.2f\", $(NF-2)}'"
  return str(subprocess.check_output(cmd, shell=True))[:-1]