from basefilter import BaseFilter
from math import pow

class Filter(BaseFilter):
  def __init__(self):
    BaseFilter.__init__(self, "Expo")

  def _impl_function(self, channel, mod):
    # K: 1, 1.5, 2, 3, 5...
    # X: -1 .. 1
    # y = (x*x*x * (k-1) + x ) / k 
    return (pow(channel, 3) * (mod - 1.0) + channel) / mod

  def _impl_getDefaultCfg(self, channel):
    return 20

  def _impl_afterConfigLoad(self):
    def convert(val):
      # value is between 0 and 100, we scale it using a quadratic function after converting to 0..1 value
      val = val / 100.0
      return 4*val*val + 1.0
    self._mods = [convert(x) for x in self._mods]

  def _impl_getCfgDescription(self):
    return "Value between 0 and 100"