from remo_launcher.msg import DisplayText
import rospy

disp_pub = None
this_menu = None
PIXEL_PER_MIL = 6


def send_msg(line, start, text, clear=False):
    msg = DisplayText()
    msg.line = line
    msg.start = start
    msg.text = text
    msg.clear = clear
    msg.header.stamp = rospy.Time.now()
    disp_pub.publish(msg)


class Menu:
    def __init__(self):
        global this_menu
        self._pages = {}
        self._current_page = None
        self._tickcounter = 0
        self._pagestack = []
        this_menu = self

    def init(self):
        global disp_pub
        disp_pub = rospy.Publisher("displaytext", DisplayText, queue_size=20)

    def tick(self):
        self._tickcounter += 1
        self._current_page.tick(self._tickcounter)

    def add_page(self, page):
        self._pages[page.name] = page

    def page_down(self, name):
        if self._current_page:
            self._pagestack.append(self._current_page.name)
        self._current_page = self._pages[name]
        self._current_page.setup()

    def page_up(self):
        if len(self._pagestack):
            prev = self._pagestack.pop()
            self._current_page = self._pages[prev]
            self._current_page.setup()


class Page:
    def __init__(self, name):
        self._todo = []
        self.name = name
        self.scroll = 0
        self._setup = []
        this_menu.add_page(self)

    def tick(self, counter):
        for mod, stuff in self._todo:
            if counter % mod == 0:
                stuff()

    def add_once(self, line, start, text):
        self._setup.append((line, start, text))

    def add_todo(self, modulo, functor):
        self._todo.append((modulo, functor))

    def setup(self):
        self.scroll = 0
        send_msg(0, 0, "", True)
        send_msg(1, 0, "", True)
        send_msg(2, 0, "", True)
        send_msg(3, 0, "", True)
        for line, start, text in self._setup:
            send_msg(line, start, text, False)
        for mod, stuff in self._todo:
            stuff()
