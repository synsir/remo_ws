from basefilter import BaseFilter
from math import pow

class Filter(BaseFilter):
  def __init__(self):
    BaseFilter.__init__(self, "Trim")

  def _impl_function(self, channel, mod):
    return channel + mod

  def _impl_getDefaultCfg(self, channel):
    return 0

  def _impl_afterConfigLoad(self):
    # after load, the config contains percentage offset values as integer (+-0..1000)
    # modify it to match the real scale of float -1.0 .. 1.0
    self._mods = [ float(x) / 1000.0 for x in self._mods ]

  def _impl_getCfgDescription(self):
    return "Value between -1000 and 1000"