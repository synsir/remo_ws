#include <ros/ros.h>
#include <remo_launcher/IntermediateChannels.h>
#include <string>
#include <vector>
#include <boost/bind.hpp>
#include "filter.h"

template <typename T>
void filter_loader(std::unique_ptr<Filter> &pf, ros::Publisher &op, const std::string &nodename = "")
{
    if (nodename.compare(T::name()) == 0 || nodename.compare("") == 0)
    {
        ROS_INFO("Loading filter: %s", T::name());
        pf.reset(new T(T::name(), op));
    }
}

int main(int argc, char **argv)
{
    std::string nodename(argv[1]);

    bool nodelet = nodename.compare("nodelet") == 0;

    ros::init(argc, argv, nodename);
    ros::NodeHandle n;

    try
    {
        std::vector<std::unique_ptr<Filter>> filterlist;
        std::unique_ptr<Filter> pfilter(nullptr);
        ros::Publisher outpub;

        if (nodelet)
        {
            ROS_INFO("Running in nodelet mode.");
            outpub = n.advertise<remo_launcher::IntermediateChannels>("dualrate_out", 1);
            filter_loader<JoyInput>(pfilter, outpub);
            std::unique_ptr<Filter> tmpfilter(nullptr);

            filter_loader<Expo>(tmpfilter, outpub);
            filterlist.push_back(std::move(tmpfilter));

            filter_loader<Trim>(tmpfilter, outpub);
            filterlist.push_back(std::move(tmpfilter));

            filter_loader<DualRate>(tmpfilter, outpub);
            filterlist.push_back(std::move(tmpfilter));

            for(auto &pf : filterlist)
            {
                pf->load_config("default");
            }
        }
        else
        {
            ROS_INFO("Trying to load Filter: %s", nodename.c_str());
            outpub = n.advertise<remo_launcher::IntermediateChannels>(nodename + "_out", 1);
            filter_loader<JoyInput>(pfilter, outpub, nodename);
            filter_loader<Expo>(pfilter, outpub, nodename);
            filter_loader<Trim>(pfilter, outpub, nodename);
            filter_loader<DualRate>(pfilter, outpub, nodename);
        }

        pfilter->load_config("default");
        ros::Subscriber insub;
        if (pfilter->hasParent())
        {
            std::string parent_topic = std::string(argv[2]) + "_out";
            insub = n.subscribe<remo_launcher::IntermediateChannels>(parent_topic, 1,
                                                                     boost::bind(&Filter::apply, boost::ref(*pfilter), _1));
        }
        else
        {
            pfilter->generator_entry(n, filterlist);
        }
        while (ros::ok())
        {
            ros::spin();
        }
    }
    catch (const std::exception &e)
    {
        ROS_FATAL("%s", e.what());
        return -1;
    }

    return 0;
}