#!/bin/bash

WORKSPACESRC="$HOME/remo_ws/src/"

if [ "$#" -eq 2 ]; then
  if [ "$1" = "cpp" ]; then
    echo "Creating c++ package with name $2 in $WORKSPACESRC"
    cd $WORKSPACESRC
    catkin_create_pkg -l MIT -a synsi -m synsi $2 roscpp std_msgs remo_launcher message_runtime
    cd $2
  elif [ "$1" = "py" ]; then
    echo "Creating python package with name $2 in $WORKSPACESRC"
    cd $WORKSPACESRC
    catkin_create_pkg -l MIT -a synsi -m synsi $2 rospy std_msgs remo_launcher message_runtime
    cd $2
  else
    echo "neither py nor cpp used as first parameter"
    exit -1
  fi
  mkdir launch
  exit 0
else
  echo "you need to supply a name for the new package and either cpp or py as first argument"
  exit 1
fi

