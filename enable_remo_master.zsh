#!/bin/bash

echo "Usage note: add an IPV4 address as argument to define the ros master, else using default"

MACHINE_IP=(`hostname -I`)
export ROS_IP=${MACHINE_IP[1]}
if [ "$#" -eq 0 ]; then
    export ROS_MASTER_URI="http://192.168.178.195:11311"
    echo "Using Default IP address: $ROS_MASTER_URI"
else 
    export ROS_MASTER_URI="http://$1:11311"
    echo "Using IP address: $ROS_MASTER_URI"
fi
