remo_ws

This project aims to use X-Box controllers and NRF24l modules connected a RPI-ZeroW t as a remote control for cars and planes or anything else. As a basic framework, ROS will be used to implement the remote control, and the receivers will be microcontrollers.

[More information about this project on my blog](https://www.himitsu.dev/ros/20/using-ros-to-build-a-remote-control/)

To launch the remote, run `roslaunch remo_launcher remo.launch`