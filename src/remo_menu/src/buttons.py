from sensor_msgs.msg import Joy
import rospy

"""
Microsoft Xbox 360 Wired Controller for Linux

Table of index number of /joy.buttons:

Index Button name on the actual controller

0 A

1 B

2 X

3 Y

4 LB

5 RB

6 back

7 start

8 power

9 Button stick left

10 Button stick right

Table of index number of /joy.axes:

Index Axis name on the actual controller

0 Left/Right Axis stick left

1 Up/Down Axis stick left

2 LT

3 Left/Right Axis stick right

4 Up/Down Axis stick right

5 RT

6 cross key left/right

7 cross key up/down 
"""

BUTTON_A = 0
BUTTON_B = 1
BUTTON_X = 2
BUTTON_Y = 3
BUTTON_LB = 4
BUTTON_RB = 5
BUTTON_BACK = 6
BUTTON_START = 7
BUTTON_POWER = 8
BUTTON_SL = 9
BUTTON_SR = 10
CROSS_UP = 11
CROSS_DOWN = 12
CROSS_LEFT = 13
CROSS_RIGHT = 14


def dummy_callback(time, state):
    pass


actions = [None] * 15
buttons = [dummy_callback] * 15
butsub = None


def check_buttons(joymsg):
    time = joymsg.header.stamp
    crosslist = [0, 0, 0, 0]
    crossud = joymsg.axes[7]
    crosslr = joymsg.axes[6]
    # cross up down
    if crossud > 0.0:
        crosslist[0] = 1
    elif crossud < 0.0:
        crosslist[1] = 1
    # cross left right
    if crosslr > 0.0:
        crosslist[2] = 1
    elif crosslr < 0.0:
        crosslist[3] = 1
    all_stuff = list(joymsg.buttons) + crosslist
    for but, state in zip(buttons, all_stuff):
        but(time, state)


def init():
    global butsub
    butsub = rospy.Subscriber("/joy", Joy, check_buttons, queue_size=1)


class Action:
    def __init__(self, functor, min_press, rep_delay, edge):
        self._duration = rospy.Duration(min_press)
        self._repetition = rospy.Duration(rep_delay) - self._duration
        self._functor = functor
        self._last_state = False
        self._last_time = rospy.Time.now()
        self._edge = edge

    def is_pressed(self, time, state):
        if state:
            if not self._last_state:
                # button just got pressed, capture time
                self._last_time = time
            elif (time - self._last_time) > self._duration:
                # button is held and duration exceeds minimum
                try:
                    self._functor()
                    self._last_time = time + self._repetition
                except Exception as e:
                    rospy.logerr("Error during button > %s", str(e))
        self._last_state = state


def add_callback(button, functor, duration=0.01, repetion=0.2, edge='falling'):
    if edge != 'falling':
        rospy.logwarn("there is no rising edge or toggle mode yet!")
        return
    act = Action(functor, duration, repetion, edge)
    actions[button] = act
    buttons[button] = act.is_pressed
